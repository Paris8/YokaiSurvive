extends Node

export (PackedScene) var Ennemi
export (PackedScene) var Coin
var score
var vie = 3
var content_music
var content

func _ready():
	var file = File.new()
	var difficulty = "0.2"
	file.open("res://settings_difficulty.txt", File.WRITE)
	file.store_string(difficulty)
	file.close()
	$TimerEnnemi.set_wait_time(float(difficulty))
	file.open("res://settings_music.txt", File.WRITE)
	file.store_string("on")
	file.close()
	randomize()

func perdu():
	$TimerScore.stop()
	$TimerEnnemi.stop()
	$TimerCoin.stop()
	get_tree().call_group("ennemis", "queue_free") # supprime tous les ennemis restants dans le groupe "ennemis"
	get_tree().call_group("coins", "queue_free") # pareil pour les pieces
	$AudioStreamPlayer.stop()
	if content_music == "on":
		$AudioGameOver.play()
	$Menu.show_partie_perdue()
	$TextureRect.texture = load("res://imgs/background.png")

func nouvelle_partie():
	score = 0
	$Joueur.start($PositionDebut.position)
	$TimerDebut.start()
	vie = 3
	$Vie.update_health(vie)
	$Menu.update_score(score)
	var file_music = File.new()
	file_music.open("res://settings_music.txt", File.READ)
	content_music = file_music.get_as_text()
	file_music.close()
	if content_music == "on":
		$AudioStreamPlayer.play()
	$AudioGameOver.stop()
	$Menu.show_message("Des ennemis arrivent...")
	$TextureRect.texture = load("res://imgs/background_ig.png")	
	# lecture des paramètres
	
	var file = File.new()
	file.open("res://settings_difficulty.txt", File.READ)
	content = file.get_as_text()
	file.close()
	$TimerEnnemi.set_wait_time(float(content))

func _on_TimerEnnemi_timeout():
	$Path2D/SpawnEnnemi.offset = randi() # Choisi un endroit aléatoire sur Path2D (autour de l'écran)
	var ennemi = Ennemi.instance() # créer une instance d'un ennemi
	add_child(ennemi) # ajout de l'ennemi à la scène
	var direction = $Path2D/SpawnEnnemi.rotation + PI / 2 # direction de l'ennemi perpendiculaire à sa trajectoire
	ennemi.position = $Path2D/SpawnEnnemi.position # fait spawn l'ennemi à l'endroit aléatoire
	direction += rand_range(-deg2rad(45), deg2rad(45)) # ajout d'un aléatoire de 90° dans la direction
	if ennemi.type_ennemi == "oeil":
		ennemi.rotation = direction # l'ennemi regarde devant lui
	ennemi.linear_velocity = Vector2(rand_range(ennemi.min_vitesse, ennemi.max_vitesse), 0) # vitesse de l'ennemi
	ennemi.linear_velocity = ennemi.linear_velocity.rotated(direction) # direction de l'ennemi

func _on_TimerDebut_timeout(): # démarre les autres timers
	$TimerEnnemi.start()
	$TimerCoin.start()
	$TimerScore.start()

func _on_TimerCoin_timeout():
	$Path2D/SpawnEnnemi.offset = randi()
	var coin = Coin.instance()
	add_child(coin)
	var direction = $Path2D/SpawnEnnemi.rotation + PI / 2
	coin.position = $Path2D/SpawnEnnemi.position
	direction += rand_range(-deg2rad(45), deg2rad(45))
	coin.linear_velocity = Vector2(rand_range(coin.min_vitesse, coin.max_vitesse), 0)
	coin.linear_velocity = coin.linear_velocity.rotated(direction)

func _on_TimerScore_timeout(): # augmente le score de 1 toutes les secondes (correspond au wait time)
	score += 1
	$Menu.update_score(score)

func piece():
	score += 5
	if content_music == "on":
		$AudioPiece.play()
	$Menu.update_score(score)
	get_tree().call_group("coins", "queue_free") # supprime les pieces

func _on_Joueur_touche():
	if not $TimerIsostasieVie.time_left > 0:
		vie -= 1
		$Vie.update_health(vie)
		$TimerIsostasieVie.start()

func _on_Menu_musicoff():
	$AudioGameOver.stop()
