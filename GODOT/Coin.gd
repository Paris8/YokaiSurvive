extends RigidBody2D

export var min_vitesse = 150
export var max_vitesse = 200

func _ready():
	$AnimatedSprite.animation = "coin"

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
