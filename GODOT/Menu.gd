extends CanvasLayer

signal lancement_partie
var difficulty_facile = "0.5"
var difficulty_normal = "0.2"
var difficulty_difficile = "0.1"
signal musicoff

func _ready():
	cacher_settings()
	AllButtonNiveauWhite()
	AllButtonMusicWhite()
	$Settings/ButtonNormal.add_color_override("font_color", Color.green)
	$Settings/ButtonMusiqueON.add_color_override("font_color", Color.green)

func show_message(text):
	$LabelMessage.text = text
	$LabelMessage.show()
	$TimerMessage.start()

func show_partie_perdue():
	$"Menu Principal/ButtonStart".show()
	$"Menu Principal/ButtonSettings".show()
	$"Menu Principal/LabelGameScore".hide()
	$"Menu Principal/LabelGameRecord".hide()
	$"Menu Principal/LabelMenuScore".show()
	$"Menu Principal/LabelMenuRecord".show()
	show_message("Perdu !")

func update_score(score):
	$"Menu Principal/LabelMenuScore".text = str(score)
	$"Menu Principal/LabelGameScore".text = str(score)

func _on_TimerMessage_timeout():
	$LabelMessage.hide()

func _on_ButtonStart_pressed():
	$"Menu Principal/ButtonStart".hide()
	emit_signal("lancement_partie")
	$"Menu Principal/ButtonSettings".hide()
	$"Menu Principal/LabelGameScore".show()
	$"Menu Principal/LabelGameRecord".show()
	$"Menu Principal/LabelMenuScore".hide()
	$"Menu Principal/LabelMenuRecord".hide()

func _on_ButtonSettings_pressed():
	for i in $"Menu Principal".get_children():
		i.hide()
	montrer_settings()

func cacher_settings(): # on cache les settings
	for i in $Settings.get_children():
		i.hide()
	
func montrer_settings(): # on affiche les settings
	for i in $Settings.get_children():
		i.show()
	$LabelMessage.hide()
	
func _on_ButtonBackToMenu_pressed():
	for i in $"Menu Principal".get_children():
		i.show()
	cacher_settings()

func _on_ButtonFacile_pressed():
	AllButtonNiveauWhite()
	var file = File.new()
	file.open("res://settings_difficulty.txt", File.WRITE)
	file.store_string(difficulty_facile)
	file.close()
	$Settings/ButtonFacile.add_color_override("font_color", Color.green)

func _on_ButtonNormal_pressed():
	AllButtonNiveauWhite()
	var file = File.new()
	file.open("res://settings_difficulty.txt", File.WRITE)
	file.store_string(difficulty_normal)
	file.close()
	$Settings/ButtonNormal.add_color_override("font_color", Color.green)

func _on_ButtonDifficile_pressed():
	AllButtonNiveauWhite()
	var file = File.new()
	file.open("res://settings_difficulty.txt", File.WRITE)
	file.store_string(difficulty_difficile)
	file.close()
	$Settings/ButtonDifficile.add_color_override("font_color", Color.green)

func AllButtonNiveauWhite():
	$Settings/ButtonFacile.add_color_override("font_color", Color.white)
	$Settings/ButtonNormal.add_color_override("font_color", Color.white)
	$Settings/ButtonDifficile.add_color_override("font_color", Color.white)

func _on_ButtonMusiqueON_pressed():
	AllButtonMusicWhite()
	$Settings/ButtonMusiqueON.add_color_override("font_color", Color.green)
	var file = File.new()
	file.open("res://settings_music.txt", File.WRITE)
	file.store_string("on")
	file.close()

func _on_ButtonMusiqueOFF_pressed():
	AllButtonMusicWhite()
	$Settings/ButtonMusiqueOFF.add_color_override("font_color", Color.red)
	var file = File.new()
	file.open("res://settings_music.txt", File.WRITE)
	file.store_string("off")
	file.close()
	emit_signal("musicoff")

func AllButtonMusicWhite():
	$Settings/ButtonMusiqueOFF.add_color_override("font_color", Color.white)
	$Settings/ButtonMusiqueON.add_color_override("font_color", Color.white)
