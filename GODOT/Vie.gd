extends HBoxContainer

var coeur_plein = preload("res://imgs/coeur.png")
var coeur_vide = preload("res://imgs/coeur_vide.png")

func update_health(value):
	for i in get_child_count():
		if value > i:
			get_child(i).texture = coeur_plein
		else:
			get_child(i).texture = coeur_vide
