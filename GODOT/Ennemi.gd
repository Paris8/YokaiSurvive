extends RigidBody2D

export var min_vitesse = 175
export var max_vitesse = 225
export var type_ennemi = "None"

func _ready():
	var type = $AnimatedSprite.frames.get_animation_names() # liste des animations
	$AnimatedSprite.animation = type[randi() % type.size()] # choix d'une animation parmis la liste
	type_ennemi = $AnimatedSprite.animation

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
