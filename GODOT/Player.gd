extends Area2D

export var vitesse = 400  # vitesse en pixel/sec
var screen_size  # taille de la fenetre
signal perdu
signal piece
signal touche
var vie = 3

func _ready():
	screen_size = get_viewport_rect().size
	hide()
	
func _process(delta):
	var velocity = Vector2(0, 0)
	var animated = false
	
	# animation diagonales
	
	if Input.is_action_pressed('ui_up') and (Input.is_action_pressed('ui_left') or Input.is_action_pressed('ui_right')):
		if $TimerIsostasieVie.time_left > 0:
			$AnimatedSprite.play("haut-hit")
		else:
			$AnimatedSprite.play("haut")
		animated = true
	if Input.is_action_pressed('ui_down') and (Input.is_action_pressed('ui_left') or Input.is_action_pressed('ui_right')):
		if $TimerIsostasieVie.time_left > 0:
			$AnimatedSprite.play("bas-hit")
		else:
			$AnimatedSprite.play("bas")
		animated = true
	
	# déplacement et animation gauche droite haut bas
	
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
		if not animated:
			if $TimerIsostasieVie.time_left > 0:
				$AnimatedSprite.play("droite-hit")
			else:
				$AnimatedSprite.play("droite")
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
		if not animated:
			if $TimerIsostasieVie.time_left > 0:
				$AnimatedSprite.play("gauche-hit")
			else:
				$AnimatedSprite.play("gauche")
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
		if not animated:
			if $TimerIsostasieVie.time_left > 0:
				$AnimatedSprite.play("bas-hit")
			else:
				$AnimatedSprite.play("bas")
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
		if not animated:
			if $TimerIsostasieVie.time_left > 0:
				$AnimatedSprite.play("haut-hit")
			else:
				$AnimatedSprite.play("haut")
	
	# vitesse indentique en diagonale
	if velocity.length() > 0:
		velocity = velocity.normalized() * vitesse
	else:
		$AnimatedSprite.set_frame(0)
		$AnimatedSprite.stop()
		
	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)

func _on_Joueur_body_entered(body): # _body au lieu de body parce que on ne l'utilise pas
	if "Ennemi" in str(body.name):
		emit_signal("touche")
		if not $TimerIsostasieVie.time_left > 0:
			vie -= 1
			$TimerIsostasieVie.start()
		if vie < 1:
			emit_signal("perdu")
			hide()
			$CollisionShape2D.set_deferred("disabled", true)
	else:
		emit_signal("piece")

func start(pos):
	position = pos
	$AnimatedSprite.play("bas")
	$AnimatedSprite.stop()
	show()
	vie = 3
	$CollisionShape2D.disabled = false

func _on_TimerIsostasieVie_timeout():
	var anim = $AnimatedSprite.get_animation()
	if "hit" in anim:
		if "gauche" in anim:
			$AnimatedSprite.play("gauche")
		if "droite" in anim:
			$AnimatedSprite.play("droite")
		if "bas" in anim:
			$AnimatedSprite.play("bas")
		if "haut" in anim:
			$AnimatedSprite.play("haut")
